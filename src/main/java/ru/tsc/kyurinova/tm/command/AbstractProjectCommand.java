package ru.tsc.kyurinova.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.exception.empty.EmptyNameException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable Project project) {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        @Nullable final List<Project> projects = serviceLocator.getProjectService().findAll(userId);
        final Integer indexNum = projects.indexOf(project) + 1;
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println("Index: " + indexNum);
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus().getDisplayName());
    }

    @NotNull
    protected Project add(@Nullable final String name, @NotNull final String description) {
        if (!Optional.ofNullable(name).isPresent() || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
